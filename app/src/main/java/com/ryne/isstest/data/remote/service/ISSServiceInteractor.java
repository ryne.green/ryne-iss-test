package com.ryne.isstest.data.remote.service;


public interface ISSServiceInteractor {

    void getISSPasses(double lat, double lon, int passes);

}
