package com.ryne.isstest.data.remote.service;

import com.ryne.isstest.model.ISSResponse;
import com.ryne.isstest.util.Constants;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ISSService implements ISSServiceInteractor {

    private ISSAPI issapi;
    private ISSServiceListener issServiceListener;

    @Inject
    public ISSService(ISSAPI issapi) {
        this.issapi = issapi;
    }

    public void setListener(ISSServiceListener issServiceListener) {
        this.issServiceListener = issServiceListener;
    }

    public void unsetListener() {
        this.issServiceListener = null;
    }

    @Override
    public void getISSPasses(double lat, double lon, int passes) {
        issapi.getISSPasses(lat, lon, passes).enqueue(new Callback<ISSResponse>() {
            @Override
            public void onResponse(Call<ISSResponse> call, Response<ISSResponse> response) {
                if(response.isSuccessful()) {
                    issServiceListener.onPassesReady(response.body());
                } else {
                    issServiceListener.onISSAPIError(Constants.ERROR_BACKEND);
                }
            }

            @Override
            public void onFailure(Call<ISSResponse> call, Throwable t) {
                issServiceListener.onNetworkError(Constants.ERROR_NETWORK);
            }
        });
    }

    public interface ISSServiceListener {
        void onPassesReady(ISSResponse issResponse);
        void onISSAPIError(int error);
        void onNetworkError(int error);
    }

}
