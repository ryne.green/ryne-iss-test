package com.ryne.isstest.data.remote.service;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ISSModule {

    @Provides
    @Singleton
    public ISSAPI provideISSService(Retrofit retrofit) {
        return retrofit.create(ISSAPI.class);
    }


    @Provides
    @Singleton
    public ISSService provideISSServiceInteractor(ISSAPI issapi) {
        return new ISSService(issapi);
    }

}
