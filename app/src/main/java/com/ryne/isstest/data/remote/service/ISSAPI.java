package com.ryne.isstest.data.remote.service;

import com.ryne.isstest.model.ISSResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ISSAPI {

    @GET("iss-pass.json")
    Call<ISSResponse> getISSPasses(@Query("lat") double lat, @Query("lon") double lon, @Query("passes") int passes);

}
