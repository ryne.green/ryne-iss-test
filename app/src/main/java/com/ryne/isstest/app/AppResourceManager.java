package com.ryne.isstest.app;

import android.content.Context;
import android.support.annotation.StringRes;

import com.ryne.isstest.R;
import com.ryne.isstest.util.Constants;

import javax.inject.Inject;

public class AppResourceManager {

    private Context context;

    @Inject
    public AppResourceManager(Context context) {
        this.context = context;
    }

    public String getErrorId(int error) {
        switch (error) {
            case Constants.ERROR_BACKEND:
                return context.getResources().getString(R.string.lbl_error_backend);
            case Constants.ERROR_NETWORK:
                return context.getResources().getString(R.string.lbl_error_network);
            default:
                return context.getResources().getString(R.string.lbl_error_general);
        }
    }

    public String getPreparedString(@StringRes int id, String value) {
        return context.getResources().getString(id, value);
    }

}
