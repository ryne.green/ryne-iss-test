package com.ryne.isstest.app;

import android.app.Application;

import com.ryne.isstest.network.NetModule;
import com.ryne.isstest.data.remote.service.ISSModule;

public class ISSApp extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent =
                DaggerAppComponent.builder()
                        .appModule(new AppModule(this))
                        .iSSModule(new ISSModule())
                        .netModule(new NetModule())
                        .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

}
