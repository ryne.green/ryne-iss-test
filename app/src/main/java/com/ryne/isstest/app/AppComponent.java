package com.ryne.isstest.app;

import com.ryne.isstest.network.NetModule;
import com.ryne.isstest.data.remote.service.ISSModule;
import com.ryne.isstest.data.remote.service.ISSService;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        AppModule.class,
        NetModule.class,
        ISSModule.class
})
public interface AppComponent {

    ISSService issService();

    AppResourceManager appRresourceMaanger();

}
