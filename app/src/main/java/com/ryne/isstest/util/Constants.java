package com.ryne.isstest.util;

public final class Constants {

    public static final int ERROR_BACKEND = 0;
    public static final int ERROR_NETWORK = 1;

    private Constants() {

    }
}
