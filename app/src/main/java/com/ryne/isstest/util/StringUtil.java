package com.ryne.isstest.util;

import android.text.format.DateFormat;

import java.util.Calendar;
import java.util.Locale;

public final class StringUtil {

    private static final int TIMESTAMP_FACTOR = 1000;

    private StringUtil() {

    }

    public static String getDate(long timestamp) {
        long actualTime = timestamp * TIMESTAMP_FACTOR;
        Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
        calendar.setTimeInMillis(actualTime);
        return DateFormat.format("dd-MM-yyyy hh:mm", calendar).toString();
    }
}
