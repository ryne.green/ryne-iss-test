package com.ryne.isstest.view.main;

import com.ryne.isstest.model.Response;

import java.util.List;

public interface MainContract {

    interface View {
        void showPasses(List<Response> passes);
        void showError(String error);
    }

    interface Presenter {
        void getPasses(double lat, double lon, int passes);
        void unbind();
    }

}
