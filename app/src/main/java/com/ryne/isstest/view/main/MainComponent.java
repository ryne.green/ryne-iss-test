package com.ryne.isstest.view.main;

import com.ryne.isstest.app.AppComponent;
import com.ryne.isstest.view.ActivityScope;

import dagger.Component;

@ActivityScope
@Component(modules = {MainModule.class}, dependencies = {AppComponent.class})
public interface MainComponent {

    void inject(MainActivity activity);

}
