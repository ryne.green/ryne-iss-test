package com.ryne.isstest.view.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ryne.isstest.R;
import com.ryne.isstest.app.AppResourceManager;
import com.ryne.isstest.model.Response;
import com.ryne.isstest.util.StringUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PassesAdapter extends RecyclerView.Adapter<PassesAdapter.ViewHolder> {

    private List<Response> passes;
    private AppResourceManager resManager;

    public PassesAdapter(List<Response> passes, AppResourceManager resManager) {
        this.passes = passes;
        this.resManager = resManager;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_pass, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Response pass = passes.get(position);
        String time = resManager.getPreparedString(R.string.lbl_item_pass_date, StringUtil.getDate(pass.getRisetime()));
        holder.passTimeTV.setText(time);
        String duration = resManager.getPreparedString(R.string.lbl_item_pass_duration, String.valueOf(pass.getDuration()));
        holder.passDurationTV.setText(duration);
    }

    @Override
    public int getItemCount() {
        return passes == null ? 0 : passes.size();
    }

    public void updateDataSet(List<Response> passes) {
        this.passes = passes;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_pass_time) TextView passTimeTV;
        @BindView(R.id.tv_pass_duration) TextView passDurationTV;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}
