package com.ryne.isstest.view.main;

import com.ryne.isstest.app.AppResourceManager;
import com.ryne.isstest.model.ISSResponse;
import com.ryne.isstest.data.remote.service.ISSService;

import javax.inject.Inject;

public class MainPresenter implements MainContract.Presenter, ISSService.ISSServiceListener {

    private MainContract.View mainView;
    private ISSService issService;
    private AppResourceManager resManager;

    @Inject
    public MainPresenter(MainContract.View mainView, ISSService issService, AppResourceManager resManager) {
        this.mainView = mainView;
        this.issService = issService;
        this.issService.setListener(this);
        this.resManager = resManager;
    }

    @Override
    public void getPasses(double lat, double lon, int passes) {
        issService.getISSPasses(lat, lon, passes);
    }

    @Override
    public void unbind() {
        issService.unsetListener();
    }

    @Override
    public void onPassesReady(ISSResponse issResponse) {
        mainView.showPasses(issResponse.getResponse());
    }

    @Override
    public void onISSAPIError(int error) {
        mainView.showError(resManager.getErrorId(error));
    }

    @Override
    public void onNetworkError(int error) {
        mainView.showError(resManager.getErrorId(error));
    }
}
